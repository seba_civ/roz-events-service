package pl.edu.agh.roz
package crawler.events

import com.avsystem.commons.rest.{DefaultRestApiCompanion, GET, Header, Path}
import pl.edu.agh.roz.crawler.events.BetApiModel._

import scala.concurrent.Future

trait BetApi {
  @GET("bookmakers") def loadProviders(implicit @Header("X-Mashape-Key") apiKey: ApiKey): Future[Map[String, ProviderModel]]
  @GET("markets") def loadMarkets(implicit @Header("X-Mashape-Key") apiKey: ApiKey): Future[Map[String, MarketModel]]
  @GET("events") def loadEvents(@Path date: String)(implicit @Header("X-Mashape-Key") apiKey: ApiKey): Future[Map[String, SimpleEventModel]]
  @GET("event") def loadEvent(@Path id: String)(implicit @Header("X-Mashape-Key") apiKey: ApiKey): Future[Map[String, EventModel]]
}

object BetApi extends DefaultRestApiCompanion[BetApi]
