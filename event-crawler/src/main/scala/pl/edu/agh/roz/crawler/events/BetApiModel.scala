package pl.edu.agh.roz
package crawler.events

import java.time.{LocalDate, LocalTime, ZoneId, ZonedDateTime}

import com.avsystem.commons.misc.Timestamp
import com.avsystem.commons.rest.RestDataCompanion
import com.avsystem.commons.rest.openapi.{RestSchema, RestStructure}
import com.avsystem.commons.serialization._

import scala.util.Try
import scala.util.control.NonFatal

object BetApiModel {
  @transparent
  case class ApiKey(value: String)
  object ApiKey extends RestDataCompanion[ApiKey] {
    implicit val keyCodec: GenKeyCodec[ApiKey] = GenKeyCodec.create(ApiKey(_), _.value)
  }

  case class DateTime(value: String, timezone: String) {
    def toTimestamp: Try[Timestamp] = Try {
      val (date, time) = value.splitAt(value.indexOf(" "))
      val instant = ZonedDateTime.of(LocalDate.parse(date.trim), LocalTime.parse(time.trim), ZoneId.of(timezone)).toInstant
      Timestamp(instant.toEpochMilli)
    }
  }
  object DateTime extends RestDataCompanion[DateTime]

  case class ProviderModel(@name("name") name: String)
  object ProviderModel extends RestDataCompanion[ProviderModel]

  case class OutcomeModel(@name("name") name: String)
  object OutcomeModel extends RestDataCompanion[OutcomeModel]

  case class MarketModel(name: String, outcomes: Map[String, OutcomeModel])
  object MarketModel extends RestDataCompanion[MarketModel]

  case class TeamModel(id: Int, name: String)
  object TeamModel extends RestDataCompanion[TeamModel]

  case class SportModel(id: Int, name: String)
  object SportModel extends RestDataCompanion[SportModel]

  case class LeagueModel(id: Int, name: String)
  object LeagueModel extends RestDataCompanion[LeagueModel]

  @transparent
  case class OutcomeOdds(odds: Map[String, Double])
  object OutcomeOdds extends RestDataCompanion[OutcomeOdds]

  @transparent
  case class FullTimeOdds(odds: Map[String, OutcomeOdds])
  object FullTimeOdds {
    val Empty = FullTimeOdds(Map.empty)

    private lazy val mapCodec = GenCodec.mapCodec[Map, String, OutcomeOdds]
    implicit lazy val codec: GenCodec[FullTimeOdds] = GenCodec.create(
      readFun = input =>
        FullTimeOdds(Try(mapCodec.read(input))
          .recover { case NonFatal(_) =>
            input.skip()
            Map.empty[String, OutcomeOdds]
          }.getOrElse(Map.empty[String, OutcomeOdds])),
      writeFun = (output, value) => mapCodec.write(output, value.odds),
    )

    implicit lazy val restStructure: RestStructure[FullTimeOdds] = RestStructure.materialize[FullTimeOdds]
    implicit lazy val restSchema: RestSchema[FullTimeOdds] = RestSchema.lazySchema(restStructure.standaloneSchema)
  }

  @transparent
  case class DownUnderOdds(odds: Map[String, Map[String, OutcomeOdds]])
  object DownUnderOdds {
    val Empty = DownUnderOdds(Map.empty)

    private lazy val mapCodec = GenCodec.mapCodec[Map, String, Map[String, OutcomeOdds]]
    implicit lazy val codec: GenCodec[DownUnderOdds] = GenCodec.create(
      readFun = input =>
        DownUnderOdds(Try(mapCodec.read(input))
          .recover { case NonFatal(_) =>
            input.skip()
            Map.empty[String, Map[String, OutcomeOdds]]
          }.getOrElse(Map.empty[String, Map[String, OutcomeOdds]])),
      writeFun = (output, value) => mapCodec.write(output, value.odds),
    )
    implicit lazy val restStructure: RestStructure[DownUnderOdds] = RestStructure.materialize[DownUnderOdds]
    implicit lazy val restSchema: RestSchema[DownUnderOdds] = RestSchema.lazySchema(restStructure.standaloneSchema)
  }

  case class OddsModel(
    @transientDefault @name("1") fullTimeOdds: FullTimeOdds = FullTimeOdds.Empty,
    @transientDefault @name("2") downUnderOdds: DownUnderOdds = DownUnderOdds.Empty,
  )
  object OddsModel extends RestDataCompanion[OddsModel]

  case class SimpleEventModel(id: String, datetime: DateTime)
  object SimpleEventModel extends RestDataCompanion[SimpleEventModel]

  case class EventModel(
    id: String,
    datetime: DateTime,
    home: TeamModel,
    away: TeamModel,
    league: LeagueModel,
    sport: SportModel,
    odds: OddsModel,
  )
  object EventModel extends RestDataCompanion[EventModel]
}
