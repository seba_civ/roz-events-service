package pl.edu.agh.roz
package crawler
package events

import java.time.LocalDate

import com.avsystem.commons.misc.{Opt, Timestamp}
import com.typesafe.scalalogging.LazyLogging
import monix.eval.Task
import monix.execution.Scheduler
import monix.reactive.Observable
import org.springframework.beans.factory.{DisposableBean, InitializingBean}
import pl.edu.agh.roz.core.db.dao.{PropertyDao, SportEventDao}
import pl.edu.agh.roz.core.db.entities._
import pl.edu.agh.roz.crawler.events.BetApiModel.EventModel

import scala.concurrent.duration._
import scala.util.Random
import scala.util.control.NonFatal

class ApiCrawler(
  clientProvider: ClientProvider,
  sportEventDao: SportEventDao,
  propertyDao: PropertyDao,
  interval: FiniteDuration = 1.hour,
  daysAhead: Int = 3,
) extends LazyLogging with InitializingBean with DisposableBean {
  import ApiCrawler._
  import clientProvider.apiKey

  private lazy val crawlerScheduler = Scheduler.singleThread("api-crawler")
  private lazy val client = clientProvider.clientProxy

  //TODO load from api maybe
  private val outcomesMap = Map(
    "1" -> "Home Win",
    "2" -> "Draw",
    "3" -> "Away Win",
    "4" -> "Over",
    "5" -> "Under",
  )

  def start(): Unit = {
    def runOneDay(date: LocalDate): Task[Unit] = {
      pollApi(date).onErrorRecover {
        case NonFatal(e) => logger.error("Error polling API", e)
      }
    }

    logger.info("Starting api poller")

    propertyDao.getProperty(LastPollProperty)
      .recoverNow {
        case NonFatal(e) =>
          logger.error("Error loading poll property", e)
          Opt.Empty
      }
      .mapNow(_.getOrElse(Timestamp.Zero))
      .map { lastPoll =>
        val now = Timestamp.now()
        val next = lastPoll + interval
        val delay = if (next <= now) 5.seconds else (next.millis - now.millis).millis
        crawlerScheduler.scheduleAtFixedRate(delay, interval) {
          logger.info("Polling api")
          for {
            _ <- propertyDao.saveProperty(LastPollProperty.toValue(Timestamp.now()))
              .recoverNow({ case NonFatal(_) => logger.warn("Failed to update last poll time") })
            _ <- Observable.fromIterable {
              val now = LocalDate.now()
              Range.inclusive(0, daysAhead).map(d => now.plusDays(d))
            }.delayOnNext(10.seconds).mapTask(date => runOneDay(date)).completedL.runAsync
          } yield ()
        }
      }
  }

  private def pollApi(date: LocalDate): Task[Unit] = {
    Observable.fromFuture(client.loadEvents(date.toString))
      .flatMap { events =>
        logger.info(s"Found ${events.size} events on ${date.toString}")
        Observable.fromIterator(events.valuesIterator)
      }
      .filter(_.datetime.toTimestamp.toOpt.exists(_ > Timestamp.now()))
      .take(1000) //dev hardcoded limit
      .delayOnNext(20.millis)
      .mapFuture { event =>
        client.loadEvent(event.id).mapNow(_.valuesIterator.nextOpt).recoverNow {
          case NonFatal(e) =>
            logger.warn(s"Failed to load event details for event ${event.id}", e)
            Opt.Empty
        }
      }
      .flatMap(evOpt => Observable.fromIterable(evOpt))
      .flatMap(event => Observable.fromIterable(toDbModel(event)))
      .mapTask { case (event, overwrite) =>
        if (overwrite) sportEventDao.saveEvent(event)
        else sportEventDao.exists(event.id).flatMap { exists =>
          if (!exists) sportEventDao.saveEvent(event) else Task.unit
        }
      }
      .countL.map(count => logger.info(s"Updated $count events"))
  }

  private def toDbModel(event: EventModel): Opt[(SportEventEntity, Boolean)] = {
    def parseOdds(oddsModel: Map[String, BetApiModel.OutcomeOdds], name: String): Opt[BetOdds] = {
      val outcomes = oddsModel.iterator
        .flatMap { case (outcomeId, odds) =>
          for {
            value <- odds.odds.valuesIterator.nextOpt
            outcome <- outcomesMap.getOpt(outcomeId)
          } yield BetOutcomeOdd(BetOutcomeId(outcome), value)
        }.toVector
      if (outcomes.nonEmpty) BetOdds(name, outcomes).opt
      else Opt.Empty
    }

    def parseDownUnderOdds: ISeq[BetOdds] = {
      event.odds.downUnderOdds.odds.iterator.flatMap { case (value, oddsModel) =>
        parseOdds(oddsModel, s"Down / Under $value")
      }.toVector
    }

    def randomize: Seq[BetOdds] = {
      def gen(base: Double): Double = {
        BigDecimal(base + Random.nextDouble()).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
      }

      def odds: (Double, Double, Double) = {
        (gen(1.0), gen(2.0), gen(3.0))
      }

      val (home, draw, away) = odds
      parseOdds(
        Map(
          "1" -> BetApiModel.OutcomeOdds(Map("" -> home)),
          "2" -> BetApiModel.OutcomeOdds(Map("" -> draw)),
          "3" -> BetApiModel.OutcomeOdds(Map("" -> away)),
        ),
        "Full Time Result"
      ).toSeq
    }

    for {
      time <- event.datetime.toTimestamp.toOpt
      (odds, overwrite) = {
        val odds = parseOdds(event.odds.fullTimeOdds.odds, "Full Time Result").toSeq ++ parseDownUnderOdds
        if (odds.isEmpty) {
          logger.debug(s"No odds in response for event ${event.id}. Generating fakes...")
          (randomize, false)
        } else (odds, true)
      }
    } yield {
      SportEventEntity(
        SportEventId(event.id),
        time,
        Team(event.home.name),
        Team(event.away.name),
        League(event.league.name),
        odds,
      ) -> overwrite
    }
  }

  override def afterPropertiesSet(): Unit = start()
  override def destroy(): Unit = crawlerScheduler.shutdown()
}

object ApiCrawler {
  object LastPollProperty extends TimestampProperty("LastPollTime")
}
