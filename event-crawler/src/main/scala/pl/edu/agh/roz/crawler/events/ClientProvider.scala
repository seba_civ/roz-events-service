package pl.edu.agh.roz
package crawler.events

import com.avsystem.commons.jetty.rest.RestClient
import org.eclipse.jetty.client.HttpClient
import org.eclipse.jetty.util.ssl.SslContextFactory
import org.springframework.beans.factory.{DisposableBean, InitializingBean}
import pl.edu.agh.roz.crawler.events.BetApiModel.ApiKey

class ClientProvider(rapidApiUrl: String, key: String) extends InitializingBean with DisposableBean {
  private lazy val jettyClient = new HttpClient(new SslContextFactory(true))

  implicit val apiKey: ApiKey = ApiKey(key)

  def clientProxy: BetApi = RestClient[BetApi](jettyClient, rapidApiUrl)

  override def afterPropertiesSet(): Unit = jettyClient.start()
  override def destroy(): Unit = jettyClient.stop()
}
