package pl.edu.agh.roz
package crawler
package outcome

import com.avsystem.commons.misc.Timestamp
import com.typesafe.scalalogging.LazyLogging
import monix.eval.Task
import monix.execution.Scheduler
import org.springframework.beans.factory.InitializingBean
import pl.edu.agh.roz.core.db.dao.{EventOutcomesDao, SportEventDao}
import pl.edu.agh.roz.core.db.entities.{EventOutcomeEntity, SportEventEntity}
import pl.edu.agh.roz.crawler.kafka.KafkaService

import scala.concurrent.duration._
import scala.util.control.NonFatal
import scala.util.{Failure, Random, Success}

class FakeOutcomesService(
  sportEventDao: SportEventDao,
  eventOutcomesDao: EventOutcomesDao,
  kafkaService: KafkaService,
  interval: FiniteDuration = 5.minutes,
  cancelChance: Double = 0.01,
) extends LazyLogging with InitializingBean {
  private lazy val taskScheduler = Scheduler.singleThread("outcomes-generator")

  private val matchTime = 2.hours

  def start(): Unit = {
    taskScheduler.scheduleAtFixedRate(5.seconds, interval) {
      maybeCancelEvent().runAsync.onCompleteNow {
        case Success(_) =>
        case Failure(ex) => logger.warn("Error occurred during cancelling event", ex)
      }
      generateResults(Timestamp.now()).runAsync
        .onCompleteNow {
          case Success(count) => logger.info(s"Fake result generated for $count events")
          case Failure(ex) => logger.warn("Error generating events outcomes", ex)
        }
    }
  }

  private def generateResults(time: Timestamp): Task[Long] = {
    def radomized(event: SportEventEntity): EventOutcomeEntity = {
      val result = event.odds.iterator
        .map(odds => odds.name -> odds.outcomes(Random.nextInt(odds.outcomes.size)).id)
        .toMap
      EventOutcomeEntity(event.id, result)
    }

    sportEventDao.finishedEvents(time - matchTime)
      .mapTask { event =>
        val outcome = radomized(event)
        kafkaService.pushResultNotification(outcome)
          .flatMap(_ => sportEventDao.markResult(event.id))
          .flatMap(_ => eventOutcomesDao.save(outcome))
          .map(_ => true)
          .onErrorRecover {
            case NonFatal(e) =>
              logger.warn(s"Failed to generate result for event ${event.id.value}", e)
              false
          }
      }.filter(_ == true).countL
  }

  private def maybeCancelEvent(): Task[Unit] = {
    def doCancel(): Task[Unit] = {
      val time = Timestamp.now() + (6 + Random.nextInt(18)).hours
      sportEventDao.findFirstAfter(time).flatMap {
        _.map(_.id).map { id =>
          sportEventDao.cancelEvent(id).flatMap { isCancelled =>
            if (isCancelled) {
              logger.info(s"Event with id ${id.value} was cancelled")
              kafkaService.pushCancelNotification(id)
            } else Task.unit
          }
        }.getOrElse(Task.unit)
      }
    }

    if (Random.nextDouble() <= cancelChance) doCancel()
    else Task.unit
  }

  override def afterPropertiesSet(): Unit = start()
}
