package pl.edu.agh.roz
package crawler

import pl.edu.agh.roz.core.AbstractLauncher

object CrawlerLauncher extends AbstractLauncher {
  def main(args: Array[String]): Unit = {
    init(args)
  }

}
