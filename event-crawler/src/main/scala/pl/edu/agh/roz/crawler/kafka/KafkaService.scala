package pl.edu.agh.roz
package crawler
package kafka

import java.util.concurrent.Executors

import com.avsystem.commons.serialization.HasGenCodec
import com.typesafe.scalalogging.LazyLogging
import monix.eval.Task
import monix.execution.Scheduler
import monix.kafka.{KafkaProducer, KafkaProducerConfig, Serializer}
import org.springframework.beans.factory.DisposableBean
import pl.edu.agh.roz.core.db.entities.{BetOutcomeId, EventOutcomeEntity, SportEventId}
import pl.edu.agh.roz.core.spring.AppConfigProvider

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

class KafkaService(
  resultsTopic: String,
  cancelTopic: String,
  enabled: Boolean = true,
) extends LazyLogging with DisposableBean {
  private lazy val kafkaScheduler: Scheduler =
    Scheduler(ExecutionContext.fromExecutor(Executors.newFixedThreadPool(4)))

  private lazy val resultsProducer = KafkaProducer[String, EventResultMessage](loadConfig(), kafkaScheduler)
  private lazy val cancelProducer = KafkaProducer[String, EventCancelMessage](loadConfig(), kafkaScheduler)

  private def loadConfig(): KafkaProducerConfig = {
    val config = AppConfigProvider.getAppConfig.getConfig("kafka")
    KafkaProducerConfig(config, includeDefaults = false)
  }

  def pushResultNotification(result: EventOutcomeEntity): Task[Unit] = {
    if (enabled) resultsProducer.send(resultsTopic, EventResultMessage(result)).map(_ => ()) else Task.unit
  }

  def pushCancelNotification(id: SportEventId): Task[Unit] = {
    if (enabled) cancelProducer.send(cancelTopic, EventCancelMessage(id)).map(_ => ()) else Task.unit
  }

  override def destroy(): Unit = {
    val async = resultsProducer.close().runAsync
    val async2 = cancelProducer.close().runAsync
    Await.result(async, 15.seconds)
    Await.result(async2, 15.seconds)
  }
}

// outcomes example: ("Full Time Result" -> "Home Win", "Down / Under 2.5" -> "Under")
case class EventResultMessage(id: SportEventId, outcomes: Map[String, BetOutcomeId])
object EventResultMessage extends HasGenCodec[EventResultMessage] {
  def apply(outcomeEntity: EventOutcomeEntity): EventResultMessage =
    new EventResultMessage(outcomeEntity.eventId, outcomeEntity.occurredOutcomes)

  implicit val serializer: Serializer[EventResultMessage] = GenCodecKafkaSerializer.toMonixSerializer[EventResultMessage]
}

case class EventCancelMessage(id: SportEventId)
object EventCancelMessage extends HasGenCodec[EventCancelMessage] {
  implicit val serializer: Serializer[EventCancelMessage] = GenCodecKafkaSerializer.toMonixSerializer[EventCancelMessage]
}
