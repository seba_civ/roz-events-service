package pl.edu.agh.roz
package crawler
package kafka

import java.nio.charset.StandardCharsets.UTF_8

import com.avsystem.commons.serialization.GenCodec
import com.avsystem.commons.serialization.json.JsonStringOutput
import monix.kafka.Serializer
import org.apache.kafka.common.serialization.{Serializer => KafkaSeralizer}

class GenCodecKafkaSerializer[T](implicit codec: GenCodec[T]) extends KafkaSeralizer[T] {
  override def configure(configs: JMap[String, _], isKey: Boolean): Unit = ()

  override def serialize(topic: String, data: T): Array[Byte] = {
    val json = JsonStringOutput.write(data)
    json.getBytes(UTF_8)
  }

  override def close(): Unit = ()
}

object GenCodecKafkaSerializer {
  def apply[T: GenCodec]: GenCodecKafkaSerializer[T] = new GenCodecKafkaSerializer()

  def toMonixSerializer[T: GenCodec]: Serializer[T] = new Serializer[T](
    classOf[GenCodecKafkaSerializer[T]].getCanonicalName,
    classOf[GenCodecKafkaSerializer[T]],
    _ => GenCodecKafkaSerializer[T]
  )
}
