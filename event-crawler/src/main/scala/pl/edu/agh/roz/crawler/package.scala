package pl.edu.agh.roz

import java.util.concurrent.Executors

import monix.execution.Scheduler

import scala.concurrent.ExecutionContext

package object crawler {
  private implicit lazy val crawlerExecutionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(8))
  implicit lazy val monixScheduler: Scheduler = Scheduler(crawlerExecutionContext)
}
