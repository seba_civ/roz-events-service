#!/bin/bash

kubectl delete deployments event-service-crawler
kubectl delete deployments event-api-crawler
kubectl delete statefulsets mongo
kubectl delete services mongodb

