name := "events-service"

inThisBuild(Seq(
  version := "0.8.0-SNAPSHOT",
  scalaVersion := Dependencies.scalaVersion,
  organization := "pl.edu.agh.roz",
  scalacOptions ++= Seq(
    "-feature",
    "-deprecation",
    "-unchecked",
    "-language:implicitConversions",
    "-language:existentials",
    "-language:dynamics",
    "-Xfuture",
    "-Xfatal-warnings",
    "-Xlint:_,-missing-interpolator,-adapted-args,-unused"
  ),
))

val TestAndCompileDep = "test->test;compile->compile"

val commonSettings = Seq(
  moduleName := "event-service-" + moduleName.value,
  libraryDependencies ++= Dependencies.commonDeps,
  libraryDependencies ++= Dependencies.compilerPlugins,
)

lazy val root = project.in(file("."))
  .aggregate(core, api, crawler)
  .settings(
    publishArtifact := false,
  )

lazy val core = project.in(file("event-core"))
  .settings(commonSettings)
  .settings(
    libraryDependencies ++= Dependencies.coreDeps.value,
  )

lazy val dockerSettings = Seq(
  docker / dockerfile := {
    // The assembly task generates a fat JAR file
    val baseDir = baseDirectory.in(core).value
    val artifact: File = assembly.value
    val artifactTargetPath = s"/app/${artifact.name}"
    val dockerResourcesDir = baseDir / "docker-scripts"
    val dockerResourcesTargetPath = "/app/"

    new Dockerfile {
      from("openjdk:8-jre")
      add(artifact, artifactTargetPath)
      copy(dockerResourcesDir, dockerResourcesTargetPath)
      entryPoint(s"/app/entrypoint.sh")
      cmd(s"$artifactTargetPath")
    }
  },
  docker / imageNames := {
    val imgName = ImageName(
      namespace = Some("sebaciv"),
      repository = "rozrywki2018-" + moduleName.value
    )
    Seq(
      // Sets the latest tag
      imgName.copy(tag = Some("latest")),
      // Sets a name with a tag that contains the project version
      imgName.copy(tag = Some("v" + version.value))
    )
  },
)

lazy val api = project.in(file("event-api"))
  .settings(commonSettings)
  .dependsOn(core)
  .enablePlugins(DockerPlugin)
  .settings(
    libraryDependencies ++= Dependencies.coreDeps.value,
    Compile / mainClass := Some("pl.edu.agh.roz.api.ApiLauncher"),
    assembly / mainClass := Some("pl.edu.agh.roz.api.ApiLauncher"),
  )
  .settings(dockerSettings)

lazy val crawler = project.in(file("event-crawler"))
  .settings(commonSettings)
  .dependsOn(core)
  .enablePlugins(DockerPlugin)
  .settings(
    dependencyOverrides ++= Dependencies.crawlerOverrides.value,
    libraryDependencies ++= Dependencies.coreDeps.value ++ Dependencies.crawlerDeps.value,
    Compile / mainClass := Some("pl.edu.agh.roz.crawler.CrawlerLauncher"),
    assembly / mainClass := Some("pl.edu.agh.roz.crawler.CrawlerLauncher"),
  )
  .settings(dockerSettings)
