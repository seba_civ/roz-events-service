package pl.edu.agh.roz
package api

import org.springframework.context.support.GenericApplicationContext
import pl.edu.agh.roz.api.rest.RestApiServer
import pl.edu.agh.roz.core.AbstractLauncher

object ApiLauncher extends AbstractLauncher {
  override protected def startBeans(ctx: GenericApplicationContext): Unit = {
    ctx.getBean(classOf[RestApiServer]).start()
  }

  def main(args: Array[String]): Unit = {
    init(args)
  }
}
