package pl.edu.agh.roz
package api.rest

import com.avsystem.commons.rest.openapi.{Info, Server}
import com.avsystem.commons.serialization.json.JsonStringOutput
import pl.edu.agh.roz.api.model.EventsServiceApi

object DocumentationGenerator {
  def main(args: Array[String]): Unit = {
    val openapi = EventsServiceApi.openapiMetadata.openapi(
      Info("Events Service REST API", "0.1", description = "Events Service"),
      servers = List(Server("http://localhost")),
    )

    val json = JsonStringOutput.writePretty(openapi)
    //todo save to file or sth
    println(json)
  }
}
