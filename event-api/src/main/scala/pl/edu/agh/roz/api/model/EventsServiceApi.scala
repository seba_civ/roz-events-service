package pl.edu.agh.roz
package api.model

import com.avsystem.commons.rest.{DefaultRestApiCompanion, Prefix}

trait EventsServiceApi {
  @Prefix("matches") def events: EventsApi
}

object EventsServiceApi extends DefaultRestApiCompanion[EventsServiceApi]
