package pl.edu.agh.roz

import java.util.concurrent.Executors

import monix.execution.Scheduler

import scala.concurrent.ExecutionContext

package object api {
  private implicit lazy val apiExecutionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(16))
  implicit lazy val monixScheduler: Scheduler = Scheduler(apiExecutionContext)
}
