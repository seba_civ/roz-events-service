package pl.edu.agh.roz
package api.model

import com.avsystem.commons.misc.Opt
import com.avsystem.commons.rest._
import pl.edu.agh.roz.core.db.entities.SportEventId

import scala.concurrent.Future

trait EventsApi {
  @GET("") def getEvent(@Path id: SportEventId): Future[Opt[SportEvent]]
  @POST("") def findEvents(@BodyField query: EventQueryModel = EventQueryModel()): Future[Seq[SportEvent]]
}

object EventsApi extends DefaultRestApiCompanion[EventsApi]
