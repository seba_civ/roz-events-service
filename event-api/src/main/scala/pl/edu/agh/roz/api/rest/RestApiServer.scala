package pl.edu.agh.roz
package api.rest

import com.avsystem.commons.jetty.rest.RestHandler
import org.eclipse.jetty.server.Server
import pl.edu.agh.roz.api.model.EventsServiceApi

class RestApiServer(dependencies: ApiDependencies, port: Int = 9090) {
  private lazy val server = {
    val server = new Server(port)
    server.setHandler(RestHandler[EventsServiceApi](new EventsServiceEndpoint(dependencies)))
    server
  }

  def start(): Unit = {
    server.start()
  }
}
