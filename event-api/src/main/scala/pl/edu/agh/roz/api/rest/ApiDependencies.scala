package pl.edu.agh.roz.api.rest

import pl.edu.agh.roz.core.db.dao.SportEventDao

class ApiDependencies(implicit
  val sportEventDao: SportEventDao,
)
