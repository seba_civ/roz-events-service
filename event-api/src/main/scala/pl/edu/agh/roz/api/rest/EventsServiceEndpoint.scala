package pl.edu.agh.roz
package api.rest

import pl.edu.agh.roz.api.model.{EventsApi, EventsServiceApi}

class EventsServiceEndpoint(dependencies: ApiDependencies) extends EventsServiceApi {
  import dependencies._

  override lazy val events: EventsApi = new EventsEndpoint
}
