package pl.edu.agh.roz
package api.model

import com.avsystem.commons.rest.RestDataCompanion
import pl.edu.agh.roz.core.db.entities.{BetOdds, League, SportEventId, Team}

case class SportEvent(
  id: SportEventId,
  time: JDate,
  home: Team,
  away: Team,
  league: League,
  odds: Seq[BetOdds],
  cancelled: Boolean,
)
object SportEvent extends RestDataCompanion[SportEvent]
