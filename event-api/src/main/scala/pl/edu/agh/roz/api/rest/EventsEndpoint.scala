package pl.edu.agh.roz
package api
package rest

import com.avsystem.commons.misc.Opt
import com.avsystem.commons.mongo.core.ops.Filtering
import com.mongodb.BasicDBObject
import pl.edu.agh.roz.api.model.{EventQueryModel, EventsApi, SportEvent}
import pl.edu.agh.roz.core.db.dao.SportEventDao
import pl.edu.agh.roz.core.db.entities.{SportEventEntity, SportEventId}

import scala.concurrent.Future

class EventsEndpoint(implicit sportEventDao: SportEventDao) extends EventsApi {

  override def getEvent(id: SportEventId): Future[Opt[SportEvent]] = {
    sportEventDao.getEvent(id).map(_.map(toApiModel)).runAsync
  }

  override def findEvents(query: EventQueryModel): Future[Seq[SportEvent]] = {
    import SportEventEntity._

    for {
      mongoQuery <- Future.eval {
        val filters = Vector(
          query.from.map(_.toTimestamp).map(t => TimeRef.gte(t)),
          query.to.map(_.toTimestamp).map(t => TimeRef.lt(t)),
          query.cancelled.map(c => CancelledRef.equal(c)),
        ).flatten
        if (filters.nonEmpty) Filtering.and(filters: _*)
        else new BasicDBObject
      }
      result <- {
        val pagination = query.pagination
        sportEventDao.findEvents(mongoQuery)(pagination.page, pagination.pageSize, pagination.ascending)
          .map(toApiModel)
          .toListL.runAsync
      }
    } yield result
  }

  private def toApiModel(event: SportEventEntity): SportEvent = {
    SportEvent(event.id, event.time.toJDate, event.home, event.away, event.league, event.odds, event.cancelled)
  }
}
