package pl.edu.agh.roz
package api.model

import com.avsystem.commons.rest.RestDataCompanion
import com.avsystem.commons.serialization.transientDefault

case class EventQueryModel(
  @transientDefault from: Option[JDate] = None,
  @transientDefault to: Option[JDate] = None,
  @transientDefault cancelled: Option[Boolean] = None,
  @transientDefault pagination: Pagination = Pagination.Default,
)
object EventQueryModel extends RestDataCompanion[EventQueryModel]

case class Pagination(
  @transientDefault page: Int = 0,
  @transientDefault pageSize: Int = 1000,
  @transientDefault ascending: Boolean = true,
)
object Pagination extends RestDataCompanion[Pagination] {
  val Default = Pagination()
}
