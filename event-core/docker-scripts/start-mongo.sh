#!/bin/bash

/app/wait-for-it.sh mongodb1:27017 -t 60 -- echo "Mongo1 started"
/app/wait-for-it.sh mongodb2:27017 -t 60 -- echo "Mongo2 started"
/app/wait-for-it.sh mongodb3:27017 -t 60 -- echo "Mongo3 started"
