package pl.edu.agh.roz
package core
package db

import com.google.common.net.HostAndPort
import com.mongodb.ConnectionString
import com.mongodb.connection.ClusterSettings
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.connection.NettyStreamFactoryFactory
import org.mongodb.scala.{MongoClient, MongoClientSettings, MongoCredential, ServerAddress}
import org.springframework.beans.factory.config.AbstractFactoryBean

class MongoClientFactoryBean(
  nodes: JList[String],
  credentials: MongoCredential,
  connectionUrl: String,
) extends AbstractFactoryBean[MongoClient] {
  override def getObjectType: Class[MongoClient] = classOf[MongoClient]

  override def createInstance(): MongoClient = {
    connectionUrl.opt.map(buildFromConnectionUrl).getOrElse(buildFromHostList)
  }

  private def buildFromConnectionUrl(url: String): MongoClient = {
    val builder = MongoClientSettings.builder()
      .codecRegistry(DEFAULT_CODEC_REGISTRY)
      .applyConnectionString(new ConnectionString(url))
      .streamFactoryFactory(NettyStreamFactoryFactory())

    MongoClient(builder.build())
  }

  private def buildFromHostList: MongoClient = {
    val hosts: JList[ServerAddress] = nodes.iterator.asScala.map { hostPortString =>
      val hostPort = HostAndPort.fromString(hostPortString)
      if (hostPort.hasPort) {
        new ServerAddress(hostPort.getHost, hostPort.getPort)
      } else {
        new ServerAddress(hostPort.getHost)
      }
    }.to[JList]

    val builder = MongoClientSettings.builder()
      .codecRegistry(DEFAULT_CODEC_REGISTRY)
      .applyToClusterSettings((builder: ClusterSettings.Builder) => builder.hosts(hosts))
      .setup(b => credentials.opt.foreach(b.credential))

    MongoClient(builder.build())
  }
}
