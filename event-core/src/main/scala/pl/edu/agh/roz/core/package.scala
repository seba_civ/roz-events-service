package pl.edu.agh.roz

import java.util.concurrent.Executors

import com.avsystem.commons.mongo.async.MongoObservableExtensions
import monix.execution.Scheduler

import scala.concurrent.ExecutionContext

package object core extends MongoObservableExtensions {
  private implicit lazy val coreExecutionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(16))
  implicit lazy val monixScheduler: Scheduler = Scheduler(coreExecutionContext)
}
