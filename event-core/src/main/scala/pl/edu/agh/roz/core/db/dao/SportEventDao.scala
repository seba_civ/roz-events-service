package pl.edu.agh.roz
package core
package db.dao

import com.avsystem.commons.misc.{Opt, Timestamp}
import com.avsystem.commons.mongo.core.ops.Filtering
import monix.eval.Task
import monix.reactive.Observable
import org.mongodb.scala.MongoDatabase
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.model.ReplaceOptions
import pl.edu.agh.roz.core.db.dao.AbstractDao.GenCodecIndex
import pl.edu.agh.roz.core.db.entities.SportEventEntity._
import pl.edu.agh.roz.core.db.entities.{SportEventEntity, SportEventId}

class SportEventDao(
  db: MongoDatabase,
  collectionName: String = SportEventDao.CollectionName,
) extends AbstractDao[SportEventEntity](db, collectionName, Seq(GenCodecIndex(ResultRef.ascending), GenCodecIndex(TimeRef.ascending))) {

  def getEvent(id: SportEventId): Task[Opt[SportEventEntity]] = {
    Task.deferFuture(collection.find(IdRef.equal(id)).headOption().mapNow(_.toOpt))
  }

  def exists(id: SportEventId): Task[Boolean] = {
    Task.deferFuture(collection.countDocuments(IdRef.equal(id)).head().mapNow(_ > 0))
  }

  def findEvents(query: Bson)(page: Int, pageSize: Int, ascending: Boolean): Observable[SportEventEntity] = {
    collection.find(query)
      .skip(page * pageSize)
      .limit(pageSize)
      .sort(if (ascending) TimeRef.ascending else TimeRef.descending)
      .asMonix
  }

  def saveEvent(event: SportEventEntity): Task[Unit] = {
    collection.replaceOne(IdRef.equal(event.id), event, ReplaceOptions().upsert(true)).asMonix.completedL
  }

  def finishedEvents(after: Timestamp): Observable[SportEventEntity] = {
    collection.find(Filtering.and(TimeRef.lte(after), ResultRef.equal(false), CancelledRef.equal(false))).asMonix
  }

  def markResult(id: SportEventId): Task[Unit] = {
    collection.updateMany(IdRef.equal(id), ResultRef.set(true)).asMonix.completedL
  }

  def markResults(ids: Seq[SportEventId]): Task[Unit] = {
    collection.updateMany(IdRef.in(ids: _*), ResultRef.set(true)).asMonix.completedL
  }

  def cancelEvent(id: SportEventId): Task[Boolean] = Task.deferFuture {
    collection.updateOne(Filtering.and(IdRef.equal(id), CancelledRef.equal(false)), CancelledRef.set(true))
      .head().mapNow(_.getModifiedCount > 0)
  }

  def findFirstAfter(time: Timestamp): Task[Opt[SportEventEntity]] = Task.deferFuture {
    collection.find(Filtering.and(TimeRef.gte(time), CancelledRef.equal(false))).headOption().mapNow(_.toOpt)
  }

}

object SportEventDao {
  val CollectionName = "SportEvent"
}
