package pl.edu.agh.roz
package core
package db.dao

import monix.eval.Task
import org.mongodb.scala.MongoDatabase
import pl.edu.agh.roz.core.db.entities.EventOutcomeEntity

class EventOutcomesDao(
  db: MongoDatabase,
  collectionName: String = EventOutcomesDao.CollectionName,
) extends AbstractDao[EventOutcomeEntity](db, collectionName) {

  def save(outcome: EventOutcomeEntity): Task[Unit] = {
    collection.insertOne(outcome).asMonix.completedL
  }

  def save(outcomes: Seq[EventOutcomeEntity]): Task[Unit] = {
    collection.insertMany(outcomes).asMonix.completedL
  }
}

object EventOutcomesDao {
  val CollectionName = "EventOutcomes"
}
