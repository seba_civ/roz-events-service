package pl.edu.agh.roz.core
package db.entities

import com.avsystem.commons.misc.Timestamp
import com.avsystem.commons.mongo.{BsonRef, mongoId}
import com.avsystem.commons.serialization.{HasGenCodec, flatten}

import scala.reflect.ClassTag

trait TypedProperty[T] {
  type ValueType <: TypedPropertyValue[T]
  def classTag: ClassTag[ValueType]

  def name: String
  def toValue(value: T): ValueType
}

abstract class TimestampProperty(override val name: String) extends TypedProperty[Timestamp] {
  override type ValueType = TimestampPropertyValue
  override def classTag: ClassTag[TimestampPropertyValue] = implicitly[ClassTag[TimestampPropertyValue]]
  override def toValue(value: Timestamp) = TimestampPropertyValue(name, value)
}

@flatten
sealed trait TypedPropertyValue[T] {
  @mongoId def name: String
  def value: T
}

case class TimestampPropertyValue(name: String, value: Timestamp) extends TypedPropertyValue[Timestamp]

object TypedPropertyValue extends HasGenCodec[TypedPropertyValue[_]] with BsonRef.Creator[TypedPropertyValue[_]] {
  final val IdRef = ref(_.name)
}
