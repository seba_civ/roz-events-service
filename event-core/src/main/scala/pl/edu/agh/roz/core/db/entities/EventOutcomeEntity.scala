package pl.edu.agh.roz
package core
package db.entities

import com.avsystem.commons.mongo.BsonRef
import com.avsystem.commons.serialization.HasGenCodec

case class EventOutcomeEntity(eventId: SportEventId, occurredOutcomes: Map[String, BetOutcomeId])
object EventOutcomeEntity extends HasGenCodec[EventOutcomeEntity] with BsonRef.Creator[EventOutcomeEntity] {
  final val EventIdRef = ref(_.eventId)
}
