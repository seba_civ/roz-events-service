package pl.edu.agh.roz
package core

import java.util.concurrent.TimeUnit

import com.typesafe.scalalogging.LazyLogging
import org.springframework.context.support.GenericApplicationContext
import pl.edu.agh.roz.core.spring.SpringContext

import scala.io.StdIn

abstract class AbstractLauncher extends LazyLogging {
  final def init(args: Array[String]): Unit = {
    val startTime = System.nanoTime

    val config = args.applyOrElse[Int, String](0, _ => "prod")
    val custom = sys.env.get("CUSTOM_CONFIG").toOpt
    val ctx: GenericApplicationContext = SpringContext.createApplicationContext(s"$config", logger, custom)

    startBeans(ctx)

    val duration: Long = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime - startTime)
    logger.info(s"Application started in ${duration}s.")

    // wait for user input and then stop the server
    logger.info("Click `Enter` to close application...")
    StdIn.readLine()
  }

  protected def startBeans(ctx: GenericApplicationContext): Unit = ()
}
