package pl.edu.agh.roz
package core
package db

import com.github.ghik.silencer.silent
import com.mongodb.{AuthenticationMechanism, MongoCredential}
import org.springframework.beans.factory.config.AbstractFactoryBean

import scala.beans.BeanProperty

class MongoCredentialFactory extends AbstractFactoryBean[MongoCredential] {
  @BeanProperty var mechanism: AuthenticationMechanism = _
  @BeanProperty var username: String = _
  @BeanProperty var password: String = _
  @BeanProperty var source: String = "admin"
  @BeanProperty var properties: MMap[String, AnyRef] = MMap.empty[String, AnyRef]

  override def createInstance(): MongoCredential = {
    import AuthenticationMechanism._
    import MongoCredential._

    var result = (mechanism match {
      case null => createCredential(username, source, password.toCharArray)
      case GSSAPI => createGSSAPICredential(username)
      case MONGODB_CR => createMongoCRCredential(username, source, password.toCharArray)
      case MONGODB_X509 => createMongoX509Credential(username)
      case PLAIN => createPlainCredential(username, source, password.toCharArray)
      case SCRAM_SHA_1 => createScramSha1Credential(username, source, password.toCharArray)
    }): @silent
    for ((key, value) <- properties) {
      result = result.withMechanismProperty(key, value)
    }
    result
  }

  override def getObjectType: Class[_] = classOf[MongoCredential]
}
