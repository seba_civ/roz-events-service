package pl.edu.agh.roz
package core
package db.dao

import com.avsystem.commons.misc.Opt
import org.mongodb.scala.MongoDatabase
import org.mongodb.scala.model.ReplaceOptions
import pl.edu.agh.roz.core.db.entities.{TypedProperty, TypedPropertyValue}

import scala.concurrent.Future

class PropertyDao(
  db: MongoDatabase,
  collectionName: String = PropertyDao.CollectionName
) extends AbstractDao[TypedPropertyValue[_]](db, collectionName) {
  import TypedPropertyValue._

  def getProperty[T](property: TypedProperty[T]): Future[Opt[T]] = {
    implicit val ct = property.classTag
    collection.find(IdRef.equal(property.name))
      .first().headOption()
      .mapNow(_.toOpt.collect({ case value: property.ValueType => value.value }))
  }

  def saveProperty(value: TypedPropertyValue[_]): Future[Unit] = {
    collection.replaceOne(IdRef.equal(value.name), value, ReplaceOptions().upsert(true)).head().toUnit
  }
}

object PropertyDao {
  val CollectionName = "properties"
}
