package pl.edu.agh.roz.core
package db

import org.mongodb.scala.{MongoClient, MongoDatabase}
import org.springframework.beans.factory.config.AbstractFactoryBean

class MongoDatabaseFactoryBean(client: MongoClient, dbName: String) extends AbstractFactoryBean[MongoDatabase] {
  override def getObjectType: Class[MongoDatabase] = classOf[MongoDatabase]
  override def createInstance(): MongoDatabase = client.getDatabase(dbName)
}
