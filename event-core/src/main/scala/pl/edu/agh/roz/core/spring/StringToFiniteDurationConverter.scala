package pl.edu.agh.roz.core
package spring

import org.springframework.core.convert.converter.Converter

import scala.concurrent.duration.{Duration, FiniteDuration}

class StringToFiniteDurationConverter extends Converter[String, FiniteDuration] {
  override def convert(source: String): FiniteDuration = Duration(source).asInstanceOf[FiniteDuration]
}
