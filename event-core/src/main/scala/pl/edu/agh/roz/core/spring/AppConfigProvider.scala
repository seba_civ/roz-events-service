package pl.edu.agh.roz
package core
package spring

import com.typesafe.config.Config

object AppConfigProvider {
  private var appConfig: Config = _

  def getAppConfig: Config = appConfig
  private[spring] def setAppConfig(config: Config): Unit = appConfig = config
}
