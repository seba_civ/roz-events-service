package pl.edu.agh.roz
package core
package db.dao

import com.avsystem.commons.misc.Opt
import com.avsystem.commons.mongo.scala.GenCodecCollection
import com.avsystem.commons.serialization.GenCodec
import com.typesafe.scalalogging.LazyLogging
import monix.reactive.Observable
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.model.IndexOptions
import org.mongodb.scala.{MongoCollection, MongoDatabase}
import org.springframework.beans.factory.InitializingBean
import pl.edu.agh.roz.core.db.dao.AbstractDao.GenCodecIndex

import scala.reflect.ClassTag
import scala.util.{Failure, Success}

abstract class AbstractDao[T : GenCodec : ClassTag](
  db: MongoDatabase,
  collectionName: String,
  indexes: Seq[GenCodecIndex] = Seq.empty,
) extends InitializingBean with LazyLogging {

  protected lazy val collection: MongoCollection[T] = GenCodecCollection.create[T](db, collectionName)

  override def afterPropertiesSet(): Unit = {
    Observable.fromIterable(indexes).flatMap { case GenCodecIndex(bson, options) =>
      options.map(collection.createIndex(bson, _)).getOrElse(collection.createIndex(bson)).asMonix
    }.completedL.runAsync
      .onCompleteNow {
        case Success(_) =>
        case Failure(ex) => logger.error(s"Failed to create indexes on collection $collectionName", ex)
      }
  }
}

object AbstractDao {
  case class GenCodecIndex(index: Bson, options: Opt[IndexOptions] = Opt.Empty)
}
