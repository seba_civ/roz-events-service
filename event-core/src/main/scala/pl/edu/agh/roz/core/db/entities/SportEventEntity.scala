package pl.edu.agh.roz
package core
package db.entities

import com.avsystem.commons.rest.RestDataCompanion
import com.avsystem.commons.serialization.{GenKeyCodec, HasGenCodec, transparent}
import com.avsystem.commons.misc.Timestamp
import com.avsystem.commons.mongo.{BsonRef, mongoId}

@transparent
case class SportEventId(value: String) extends AnyVal
object SportEventId extends RestDataCompanion[SportEventId] {
  implicit val keyCodec: GenKeyCodec[SportEventId] = GenKeyCodec.create(SportEventId(_), _.value)
}

case class Team(name: String)
object Team extends RestDataCompanion[Team]

case class League(name: String)
object League extends RestDataCompanion[League]

@transparent
case class BetOutcomeId(value: String) extends AnyVal
object BetOutcomeId extends RestDataCompanion[BetOutcomeId]

case class BetOutcomeOdd(id: BetOutcomeId, value: Double)
object BetOutcomeOdd extends RestDataCompanion[BetOutcomeOdd]

case class BetOdds(name: String, outcomes: Seq[BetOutcomeOdd])
object BetOdds extends RestDataCompanion[BetOdds]

case class SportEventEntity(
  @mongoId id: SportEventId,
  time: Timestamp,
  home: Team,
  away: Team,
  league: League,
  odds: Seq[BetOdds],
  cancelled: Boolean = false,
  result: Boolean = false,
)
object SportEventEntity extends HasGenCodec[SportEventEntity] with BsonRef.Creator[SportEventEntity] {
  final val IdRef = ref(_.id)
  final val TimeRef = ref(_.time)
  final val HomeRef = ref(_.home)
  final val AwayRef = ref(_.away)
  final val LeagueRef = ref(_.league)
  final val OddsRef = ref(_.odds)
  final val CancelledRef = ref(_.cancelled)
  final val ResultRef = ref(_.result)
}
