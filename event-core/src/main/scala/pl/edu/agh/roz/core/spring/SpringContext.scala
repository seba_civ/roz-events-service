package pl.edu.agh.roz
package core
package spring

import com.avsystem.commons.misc.Opt
import com.avsystem.commons.spring.{HoconBeanDefinitionReader, ScalaDefaultValuesInjector, ScalaParameterNameDiscoverer}
import com.typesafe.config.{ConfigFactory, ConfigParseOptions, ConfigResolveOptions, ConfigSyntax}
import com.typesafe.scalalogging.Logger
import org.springframework.beans.factory.support.DefaultListableBeanFactory
import org.springframework.context.support.{ConversionServiceFactoryBean, GenericApplicationContext}

import scala.util.control.NonFatal

object SpringContext {

  def createApplicationContext(configName: String, logger: Logger, customConfig: Opt[String]): GenericApplicationContext = {
    val beanFactory = new DefaultListableBeanFactory
    beanFactory.setParameterNameDiscoverer(new ScalaParameterNameDiscoverer)
    beanFactory.setConversionService(new ConversionServiceFactoryBean().setup(_.setConverters(JSet(
      new StringToFiniteDurationConverter
    ))).setup(_.afterPropertiesSet()).getObject)

    val ctx = new GenericApplicationContext(beanFactory)
    ctx.addBeanFactoryPostProcessor(new ScalaDefaultValuesInjector)
    ctx.setAllowCircularReferences(false)

    val bdr = new HoconBeanDefinitionReader(ctx)
    val beanConfig = ConfigFactory.load(
      s"$configName/beans.conf",
      ConfigParseOptions.defaults(),
      ConfigResolveOptions.defaults().setAllowUnresolved(true),
    )

    val appConfig = ConfigFactory.load(s"$configName/application.conf")
    val fullAppConfig = try {
      customConfig.map { c =>
        val custom = ConfigFactory.parseString(c, ConfigParseOptions.defaults().setSyntax(ConfigSyntax.CONF))
        custom.withFallback(appConfig)
      }.getOrElse(appConfig)
    } catch {
      case NonFatal(e) =>
        logger.warn("Failed to parse custom config", e)
        appConfig
    }
    AppConfigProvider.setAppConfig(fullAppConfig)

    val fullConfig = beanConfig.resolveWith(fullAppConfig)
    bdr.loadBeanDefinitions(fullConfig)
    ctx.refresh()
    ctx
  }

}
