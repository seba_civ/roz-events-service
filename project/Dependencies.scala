import sbt._

object Dependencies {
  val scalaVersion = "2.12.7"

  val commonsVersion = "1.34.4"
  val silencerVersion = "1.2"
  val monixVersion = "2.3.3"
  val jettyVersion = "9.4.10.v20180503"
  val springVersion = "4.3.13.RELEASE"
  val mongoVersion = "3.8.2"
  val mongoScalaVersion = "2.4.2"
  val nettyVersion = "4.1.17.Final"
  
  val kafkaVersion = "2.1.0"
  val monixKafkaVersion = "0.17"

  val slf4jVersion = "1.7.25"
  val log4jToSlf4jVersion = "2.5"
  val logbackVersion = "1.2.3"
  val scalaLoggingVersion = "3.9.0"

  val scalaTestVersion = "3.0.5"

  val compilerPlugins = Seq(
    "com.avsystem.commons" %% "commons-analyzer" % commonsVersion,
    "com.github.ghik" %% "silencer-plugin" % silencerVersion,
  ).map(compilerPlugin)
  
  val commonDeps = Seq(
    "com.github.ghik" %% "silencer-lib" % silencerVersion,
  )
  
  val coreDeps = Def.setting(Seq(
    "org.eclipse.jetty" % "jetty-server" % jettyVersion,
    "org.eclipse.jetty.websocket" % "websocket-server" % jettyVersion,
    "io.netty" % "netty-all" % nettyVersion,
    
    "com.avsystem.commons" %% "commons-core" % commonsVersion,
    "com.avsystem.commons" %% "commons-mongo" % commonsVersion,
    "com.avsystem.commons" %% "commons-jetty" % commonsVersion,
    "com.avsystem.commons" %% "commons-rest" % commonsVersion,

    "org.mongodb" % "mongodb-driver-core" % mongoVersion,
    "org.mongodb" % "mongodb-driver" % mongoVersion,
    "org.mongodb" % "mongodb-driver-async" % mongoVersion,
    "org.mongodb.scala" %% "mongo-scala-driver" % mongoScalaVersion,

    "org.springframework" % "spring-core" % springVersion,
    "org.springframework" % "spring-beans" % springVersion,
    "org.springframework" % "spring-context-support" % springVersion,

    "com.avsystem.commons" %% "commons-spring" % commonsVersion,

    "io.monix" %% "monix" % monixVersion,

    "org.slf4j" % "slf4j-api" % slf4jVersion,
    "org.slf4j" % "log4j-over-slf4j" % slf4jVersion,
    "org.apache.logging.log4j" % "log4j-to-slf4j" % log4jToSlf4jVersion,
    "ch.qos.logback" % "logback-core" % logbackVersion,
    "ch.qos.logback" % "logback-classic" % logbackVersion,
    "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion,

    "org.scalatest" %% "scalatest" % scalaTestVersion % "test",
  ))
  
  val crawlerDeps = Def.setting(Seq(
    "org.apache.kafka" %% "kafka" % kafkaVersion,
    "io.monix" %% "monix-kafka-1x" % monixKafkaVersion,
  ))
  
  val crawlerOverrides = Def.setting(Seq(
    "org.apache.kafka" %% "kafka" % kafkaVersion,
  ))

}
